package com.zuitt.wdc044.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity //mark this class as a representation of a database table
@Table(name="users") //to just name the table


public class User {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String username;

    @Column
    private String password;

    @OneToMany(mappedBy = "user")
    @JsonIgnore //to avoid infinite nesting when retrieving post content
    private Set<Post> posts;


    //default constructor
    public User(){}

    //parameterized constructor
    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    //[getters]
    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Long getId() {
        return id;
    }

    public Set<Post> getPosts() {
        return posts;
    }

    //[setters]
    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    //User model properties
    //id - long/primary key/auto-increment
    //username - String
    //password - string
    //getter setters for username and password
    //getter of id
}
