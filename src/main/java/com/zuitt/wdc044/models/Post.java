package com.zuitt.wdc044.models;

import javax.persistence.*;

@Entity //mark this class as a representation of a database table
@Table(name="posts") //to just name the table
public class Post {
    @Id //indicate that this property represents the primary key
    @GeneratedValue //auto increment
    private Long id;

    @Column
    private String title;

    @Column String content;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private  User user;

    //default constructor
    public Post(){}

    //parameterized constructor
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    //getters
    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public User getUser() {
        return user;
    }

    //setters
    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
